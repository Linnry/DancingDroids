use std::io;

//Crée la structure qui sauvagarde les données des robots
struct robot{
    pos_x : i32,
    pos_y : i32,
    name : i32,
    orientation : Orientation,

}

 //L'enum des orientations 
enum Orientation {
    North,
    East,
    South,
    West,
    }   

//L'implématation
impl Orientation   {
    fn cardiaux(&self) -> &str {
        match self{
            Orientation::North => "N",
            Orientation::East => "E",
            Orientation::South => "S",
            Orientation::West => "W",
        }
    }
}


fn main() {
//Instructions du Joueur
let mut instruction = String::new();


io::stdin()
        .read_line(&mut instruction)
        .expect("Failed to read line");

for lettre in instruction.chars(){
    if lettre == 'F'|| lettre == 'f'{
        println!("Je marche devant {}",lettre);
    }
    else if lettre == 'R'||lettre == 'r'{
        println!("Je tourne a droite  {}",lettre);
    }
    else if lettre == 'L'||lettre == 'l'{
        println!("Je tourne a gauche {}",lettre);
    }
}


}
}
